<?php
/**
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 12.11.13
 * @time 12:45
 */

namespace NNPro\IframeBundle\Listener;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;

class RedirectListener
{
    /**
     * @var string
     */
    private $nickname;

    /**
     * @param string $nickname
     */
    public function __construct($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @param FilterResponseEvent $event
     *
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            // we only work on master request
            return;
        }

        $request = $event->getRequest();


        if (!($nickname = $request->query->get($this->nickname))) {
            // nothing to do
            return;
        }

        if ($event->getResponse() instanceof RedirectResponse) {
            /** @var RedirectResponse $response */
            $response = $event->getResponse();

            $url = $response->getTargetUrl();
            $info = parse_url($url);

            $query = isset($info['query']) ? $info['query'] : '';

            if (!$query || false === strpos($query, $this->nickname)) {
                // append the query param
                $queryPart = sprintf("%s=%s", $this->nickname, $nickname);
                if (!$query) {
                    $url .= '?' . $queryPart;
                } else {
                    $url .= '&' . $queryPart;
                }

                $response->setTargetUrl($url);
                $event->setResponse($response);
            }
        }
    }
} 