<?php
/**
 * generates the socket URL to use
 *
 * @package rentorder
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 12.11.13
 * @time 11:01
 */

namespace NNPro\IframeBundle\Service;


use Symfony\Component\HttpFoundation\Request;

class Websocket
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->config = $config;
        $this->request = $request;
    }

    /**
     *
     * @return string
     */
    public function getSocketUrl()
    {
        return sprintf('%s://%s:%d',
            $this->request->getScheme(),
            $this->config['host'],
            ('http' === $this->request->getScheme() ? $this->config['port'] : $this->config['sslPort'])
        );
    }
} 