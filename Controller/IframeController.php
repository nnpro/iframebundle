<?php

namespace NNPro\IframeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class IframeController extends Controller
{
    /**
     * handles resizing the iframe from the outer frame
     *
     * @Route("/resize/{nickname}/{frame_id}.{_format}", name="nn_pro_iframe_resize",
     *  requirements={"nickname"="[a-z0-9]+", "_format"="js", "frame_id"="[a-zA-Z0-9_-]+"}
     * )
     * @Template("NNProIframeBundle:Iframe:resize.js.twig")
     */
    public function resizeAction($nickname, $frame_id)
    {
        return array(
            'socketUrl' => $this->getSocketUrl(),
            'nickname'  => $nickname,
            'frameId'   => $frame_id,
        );
    }

    /**
     * checks frame height constantly and notifies the outer frame
     *
     * @Route("/check/{frame_id}", name="nn_pro_iframe_check",
     *  requirements={"frame_id"="[a-zA-Z0-9]+"}
     * )
     * @Template()
     */
    public function checkAction($frame_id, Request $request)
    {
        $nicknameParam = $this->container->getParameter('nn_pro_iframe.nickname');
        $nickname = $request->attributes->get($nicknameParam);

        $selector = $request->attributes->get('selector', 'body');

        if ($nickname && !preg_match('#^[a-z0-9]+$#', $nickname)) {
            throw new \InvalidArgumentException('invalid nickname provided');
        }

        return array(
            'nickname'      => $nickname,
            'socketUrl'     => $this->getSocketUrl(),
            'frameId'       => $frame_id,
            'nicknameParam' => $nicknameParam,
            'selector'      => $selector,
        );
    }

    /**
     *
     * @return string
     */
    private function getSocketUrl()
    {
        return $this->get('nn_pro_iframe.websocket')->getSocketUrl();
    }
}
