<?php

namespace NNPro\IframeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nn_pro_iframe');

        $rootNode->children()
            ->arrayNode('websocket')->isRequired()
                ->children()
                    ->scalarNode('host')->isRequired()->cannotBeEmpty()->end()
                    ->integerNode('port')->defaultValue(80)->cannotBeEmpty()->end()
                    ->integerNode('sslPort')->defaultValue(443)->cannotBeEmpty()->end()
                ->end()
            ->end()
            ->scalarNode('nickname')->defaultValue('_nickname')->cannotBeEmpty()->end()
        ->end();

        return $treeBuilder;
    }
}
